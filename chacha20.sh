#!/bin/sh

# https://tools.ietf.org/html/rfc8439

readonly max32=$(( (1 << 32) -1 ))
readonly zeroToFifteen="$(seq 0 15)"

type log >/dev/null 2>&1 || log() {
	printf -- '%s\n' "$*"
} >&2
type fatal >/dev/null 2>&1 || fatal() {
	log FATAL "$@"
	exit 1
}

##### helpers #########################

printHex() {
	printf -- '%08x\n' $1
}

printState() {
	for i in $zeroToFifteen ; do
		eval printHex \$$1$i
	done
}

##### crypto ##########################

rollleft() {
	# $1 target variable
	# $2 how much to shift
	# : $(( $1 = $1 << $2 ))
	# : $(( $1 = (( $1 >> 32 ) | $1 ) & max32 ))
	: $(( $1 = (($1 << $2) & max32) | ($1 >> (32 - $2)) ))
}

sixteenthround() {
	: $(( $1 = ($1 + $2) & max32 ))
	: $(( $3 ^= $1 ))
	rollleft $3 $4
}

quarterround() {
	# a += b; d ^= a; d <<<= 16;
	# c += d; b ^= c; b <<<= 12;
	# a += b; d ^= a; d <<<= 8;
	# c += d; b ^= c; b <<<= 7;
	sixteenthround $1 $2 $4 16
	sixteenthround $3 $4 $2 12
	sixteenthround $1 $2 $4 8
	sixteenthround $3 $4 $2 7
}

state_quarterround() {
	quarterround state$1 state$2 state$3 state$4
}

doubleround() {
	state_quarterround 0 4 8 12
	state_quarterround 1 5 9 13
	state_quarterround 2 6 10 14
	state_quarterround 3 7 11 15
	state_quarterround 0 5 10 15
	state_quarterround 1 6 11 12
	state_quarterround 2 7 8 13
	state_quarterround 3 4 9 14
}

copyState() {
	# $1 src
	# $2 dst
	for i in $zeroToFifteen ; do
		eval $2$i=\$$1$i
	done
}

addStates() {
	# $1 target
	# $2 delta
	for i in $zeroToFifteen ; do
		eval $1$i="\$(( ( $1$i + $2$i ) & max32 ))"
	done
}

chacha20block() {
	copyState state initial
	doubleround; doubleround; doubleround; doubleround; doubleround;
	doubleround; doubleround; doubleround; doubleround; doubleround;
	addStates state initial
}

isHexBitsLong() {
	# 4 bits per hex digit
	[ ${#1} -eq $(($2/4)) ] \
	&& [ "$(printf -- '%s' "$1" | tr -d '[:xdigit:]')" = '' ]
	# no non-hex digits allowed
}

initialState() {
	local key="$(printf -- '%s' "$1" | tr -d ':[:space:]')"
	local nonce="$(printf -- '%s' "$2" | tr -d ':[:space:]')"

	# key & nonce must only be hex digits
	isHexBitsLong "$key" 256 || fatal "bad key: '$key'"
	isHexBitsLong "$nonce" 96  || fatal "bad nonce: '$nonce'"

	# cccccccc  cccccccc  cccccccc  cccccccc   4 * 32 bit constants
	# kkkkkkkk  kkkkkkkk  kkkkkkkk  kkkkkkkk   256 bit key
	# kkkkkkkk  kkkkkkkk  kkkkkkkk  kkkkkkkk
	# bbbbbbbb  nnnnnnnn  nnnnnnnn  nnnnnnnn   32 bit counter, 96 bit nonce

	state0='0x61707865'
	state1='0x3320646e'
	state2='0x79622d32'
	state3='0x6b206574'

	local cmd="$(
		printf -- "%s%08d%s" "$key" '0' "$nonce" \
		| sed -E 's/(..)(..)(..)(..)/0x\4\3\2\1\n/g' \
		| nl -w1 -s= -v4 \
		| sed -e 's/^/state/' \
	)"
	eval "$cmd"
	state12=1
}

##### driver ##########################

# works one BYTE at a time -> hella slow

# head -c 1M < /dev/zero | ./chacha20.sh pass nonce | dd bs=4K if=/proc/self/fd/0 of=/dev/null
# 256+0 records in
# 256+0 records out
# 1048576 bytes (1.0 MB, 1.0 MiB) copied, 40.0371 s, 26.2 kB/s

# time head -c 1M < /dev/zero | ./chacha20.sh pass nonce > /dev/null
# real	0m39.437s
# user	0m49.686s
# sys 	0m15.180s

keyStream() {
	# outputs list of hex bytes, one per line
	initialState "$1" "$2"
	copyState state keyState
	keyState12=0
	while : ; do
		: $(( keyState12 = ( keyState12 + 1 ) & max32 ))
		copyState keyState state
		chacha20block
		printState state
	done \
	| sed -E 's/(..)(..)(..)(..)/\4\n\3\n\2\n\1/'
}

inStream() {
	# outputs list of hex bytes, one per line
	od -t x1 -w1 -v -An | cut -c2-
}

encryptStdIn() {
	# outputs list of hex bytes, one per line
	{
		keyStream "$1" "$2" | {
			inStream <&5 |
			while read inByte && read keyByte <&4 ; do
				printf '%02x\n' $(( 0x$inByte ^ 0x$keyByte ))
			done
		} 4<&0
	} 5<&0
}

encryptStdIn_OutBytes() {
	# outputs raw bytes
	encryptStdIn "$1" "$2" | tr -dc '[:xdigit:]' | xxd -p -r
}

encryptStdIn_OutBytes_pbk() {
	# outputs raw bytes
	encryptStdIn_OutBytes \
		"$(echo "$1" | sha256sum | cut -c -$((256*2/8)) )" \
		"$(echo "$2" | sha256sum | cut -c -$((96*2/8)) )"
}

##### main ############################

if [ $# -eq 2 ] ; then
	encryptStdIn_OutBytes_pbk "$1" "$2"
	exit $?
fi

#######################################
##### tests ###########################
#######################################

exec 1>&2

readonly diffCmd='diff -U1 /proc/self/fd/3 -'

echo "Running tests..."

echo rollleft_test
rollleft_test() {
	local x=0x7998bfda
	rollleft x 7
	printHex $x | {
		echo 'cc5fed3c' | $diffCmd
	} 3<&0 || fatal rollleft_test
}
rollleft_test
unset rollleft_test

echo quarterround_test
quarterround_test() {
	local a b c d
	a=0x11111111 ; b=0x01020304 ; c=0x9b8d6f43 ; d=0x01234567
	quarterround a b c d
	for i in a b c d ; do eval printHex \$$i ; done | {
		printf -- '%s\n%s\n%s\n%s\n' ea2a92f4 cb1cf8ce 4581472e 5881c4bb | $diffCmd
	} 3<&0 || fatal quarterround_test
}
quarterround_test
unset quarterround_test

echo state_quarterround_test
state_quarterround_test() {
	local state='
		879531e0  c5ecf37d  516461b1  c9a62f8a
		44c20ef3  3390af7f  d9fc690b  2a5f714c
		53372767  b00a5631  974c541a  359e9963
		5c971061  3d631689  2098d9d6  91dbd320
	'
	local exp='
		879531e0  c5ecf37d  bdb886dc  c9a62f8a
		44c20ef3  3390af7f  d9fc690b  cfacafd2
		e46bea80  b00a5631  974c541a  359e9963
		5c971061  ccc07c79  2098d9d6  91dbd320
	'
	local hex
	i=-1
	for hex in $state ; do
		eval state$((i += 1))=\$"((0x$hex))"
	done
	state_quarterround 2 7 8 13
	printState state | {
		printf -- '%s\n' "$exp" | tr -c '[:xdigit:]' '\n' | grep '\w' | $diffCmd
	} 3<&0 || fatal state_quarterround_test
}
state_quarterround_test
unset state_quarterround_test

echo chacha20block_test
chacha20block_test() {
	local state='
		61707865  3320646e  79622d32  6b206574
		03020100  07060504  0b0a0908  0f0e0d0c
		13121110  17161514  1b1a1918  1f1e1d1c
		00000001  09000000  4a000000  00000000
	'
	local exp='
		e4e7f110  15593bd1  1fdd0f50  c47120a3
		c7f4d1c7  0368c033  9aaa2204  4e6cd4c3
		466482d2  09aa9f07  05d7c214  a2028bd9
		d19c12b5  b94e16de  e883d0cb  4e3c50a2
	'
	local hex
	i=-1
	for hex in $state ; do
		eval state$((i += 1))=\$"((0x$hex))"
		eval initial$i=\$state$i
	done
	chacha20block
	printState state | {
		printf -- '%s\n' "$exp" | tr -c '[:xdigit:]' '\n' | grep '\w' | $diffCmd
	} 3<&0 || fatal chacha20block_test
}
chacha20block_test
unset chacha20block_test

echo initialState_test
initialState_test() {
	local key='00:01:02:03:04:05:06:07:08:09:0a:0b:0c:0d:0e:0f
		:10:11:12:13:14:15:16:17:18:19:1a:1b:1c:1d:1e:1f'
	local nonce='00:00:00:00:00:00:00:4a:00:00:00:00'

	initialState "$key" "$nonce"
	local exp='
		61707865  3320646e  79622d32  6b206574
		03020100  07060504  0b0a0908  0f0e0d0c
		13121110  17161514  1b1a1918  1f1e1d1c
		00000001  00000000  4a000000  00000000
	'
	printState state | {
		printf -- '%s\n' "$exp" | tr -c '[:xdigit:]' '\n' | grep '\w' | $diffCmd
	} 3<&0 || fatal initialState_test
}
initialState_test
unset initialState_test

echo encryptStdIn_test
encryptStdIn_test() {
	local key='00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01'
	local nonce='00 00 00 00 00 00 00 00 00 00 00 02'
	local plain='Any submission to the IETF intended by the Contributor for publication as all or part of an IETF Internet-Draft or RFC and any statement made within the context of an IETF activity is considered an "IETF Contribution". Such statements include oral statements in IETF sessions, as well as written and electronic communications made at any time or place, which are addressed to'
	local cipher="$(cat <<- EOF
		a3 fb f0 7d f3 fa 2f de 4f 37 6c a2 3e 82 73 70
		41 60 5d 9f 4f 4f 57 bd 8c ff 2c 1d 4b 79 55 ec
		2a 97 94 8b d3 72 29 15 c8 f3 d3 37 f7 d3 70 05
		0e 9e 96 d6 47 b7 c3 9f 56 e0 31 ca 5e b6 25 0d
		40 42 e0 27 85 ec ec fa 4b 4b b5 e8 ea d0 44 0e
		20 b6 e8 db 09 d8 81 a7 c6 13 2f 42 0e 52 79 50
		42 bd fa 77 73 d8 a9 05 14 47 b3 29 1c e1 41 1c
		68 04 65 55 2a a6 c4 05 b7 76 4d 5e 87 be a8 5a
		d0 0f 84 49 ed 8f 72 d0 d6 62 ab 05 26 91 ca 66
		42 4b c8 6d 2d f8 0e a4 1f 43 ab f9 37 d3 25 9d
		c4 b2 d0 df b4 8a 6c 91 39 dd d7 f7 69 66 e9 28
		e6 35 55 3b a7 6c 5c 87 9d 7b 35 d4 9e b2 e6 2b
		08 71 cd ac 63 89 39 e2 5e 8a 1e 0e f9 d5 28 0f
		a8 ca 32 8b 35 1c 3c 76 59 89 cb cf 3d aa 8b 6c
		cc 3a af 9f 39 79 c9 2b 37 20 fc 88 dc 95 ed 84
		a1 be 05 9c 64 99 b9 fd a2 36 e7 e8 18 b0 4b 0b
		c3 9c 1e 87 6b 19 3b fe 55 69 75 3f 88 12 8c c0
		8a aa 9b 63 d1 a1 6f 80 ef 25 54 d7 18 9c 41 1f
		58 69 ca 52 c5 b8 3f a3 6f f2 16 b9 c1 d3 00 62
		be bc fd 2d c5 bc e0 91 19 34 fd a7 9a 86 f6 e6
		98 ce d7 59 c3 ff 9b 64 77 33 8f 3d a4 f9 cd 85
		14 ea 99 82 cc af b3 41 b2 38 4d d9 02 f3 d1 ab
		7a c6 1d d2 9c 6f 21 ba 5b 86 2f 37 30 e3 7c fd
		c4 fd 80 6c 22 f2 21
	EOF
	)"

	printf '%s\n' "$cipher" | tr ' ' '\n' | {
		printf '%s' "$plain" | encryptStdIn "$key" "$nonce" | $diffCmd
	} 3<&0 || fatal encryptStdIn_test
}
encryptStdIn_test
unset encryptStdIn_test

echo encryptStdIn_OutBytes_pbk_test_foo
encryptStdIn_OutBytes_pbk_test_foo() {
	local val=
	val="$(
		printf '%s' 'foo' \
		| encryptStdIn_OutBytes_pbk pass1 nonce1 \
		| encryptStdIn_OutBytes_pbk pass1 nonce1
	)"
	[ "$val" = 'foo' ] || fatal encryptStdIn_OutBytes_pbk_test_foo
}
encryptStdIn_OutBytes_pbk_test_foo
unset encryptStdIn_OutBytes_pbk_test_foo

echo encryptStdIn_OutBytes_pbk_test_0
encryptStdIn_OutBytes_pbk_test_0() {
	cat "$0" \
		| encryptStdIn_OutBytes_pbk pass2 nonce2 \
		| encryptStdIn_OutBytes_pbk pass2 nonce2 \
		| cmp - "$0" \
		|| fatal encryptStdIn_OutBytes_pbk_test_\$0
}
encryptStdIn_OutBytes_pbk_test_0
unset encryptStdIn_OutBytes_pbk_test_0

echo "...tests passed."
